USE [master]
GO
/****** Object:  Database [WuyeProject]    Script Date: 2023/1/28 23:34:38 ******/
CREATE DATABASE [WuyeProject]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'WuyeProject', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\WuyeProject.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'WuyeProject_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\WuyeProject_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [WuyeProject] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [WuyeProject].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [WuyeProject] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [WuyeProject] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [WuyeProject] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [WuyeProject] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [WuyeProject] SET ARITHABORT OFF 
GO
ALTER DATABASE [WuyeProject] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [WuyeProject] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [WuyeProject] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [WuyeProject] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [WuyeProject] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [WuyeProject] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [WuyeProject] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [WuyeProject] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [WuyeProject] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [WuyeProject] SET  DISABLE_BROKER 
GO
ALTER DATABASE [WuyeProject] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [WuyeProject] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [WuyeProject] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [WuyeProject] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [WuyeProject] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [WuyeProject] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [WuyeProject] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [WuyeProject] SET RECOVERY FULL 
GO
ALTER DATABASE [WuyeProject] SET  MULTI_USER 
GO
ALTER DATABASE [WuyeProject] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [WuyeProject] SET DB_CHAINING OFF 
GO
ALTER DATABASE [WuyeProject] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [WuyeProject] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [WuyeProject] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [WuyeProject] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'WuyeProject', N'ON'
GO
ALTER DATABASE [WuyeProject] SET QUERY_STORE = OFF
GO
USE [WuyeProject]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[RoomHAO] [varchar](50) NULL,
	[Phone] [varchar](11) NULL,
	[Mian] [varchar](50) NULL,
	[LY_info] [varchar](50) NULL,
	[AccountBalance] [decimal](18, 4) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_admin]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_admin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Pass] [varchar](50) NOT NULL,
	[NickName] [varchar](50) NULL,
	[Power] [int] NULL,
	[CreaTetime] [date] NULL,
 CONSTRAINT [PK_w] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

iNSERT INTO w_admin VALUES ('admin',123456,'管理员',1,'2022-12-01')
/****** Object:  Table [dbo].[w_charge]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_charge](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FJ_Number] [varchar](50) NULL,
	[YF_Amount] [decimal](18, 4) NULL,
	[SF_Amount] [decimal](18, 4) NULL,
	[SF_Type] [varchar](50) NULL,
	[SF_YF] [int] NULL,
	[SF_State] [varchar](50) NULL,
	[HandledBy] [varchar](50) NULL,
 CONSTRAINT [PK_w_charge] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_chargedetailed]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_chargedetailed](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UsersId] [int] NULL,
	[ZC_Amount] [decimal](18, 4) NULL,
	[ZC_Date] [datetime] NULL,
	[Remarks] [varchar](50) NULL,
	[ZC_Type] [varchar](50) NULL,
	[Surplus
Amount] [decimal](18, 4) NULL,
 CONSTRAINT [PK_w_chargedetailed] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_complaint]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_complaint](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TS_title] [varchar](50) NULL,
	[TS_unit] [varchar](50) NULL,
	[TS_Name] [varchar](50) NULL,
	[TS_date] [datetime] NULL,
	[TS_content] [varchar](200) NULL,
	[Handle] [varchar](200) NULL,
	[CL_state] [int] NULL,
 CONSTRAINT [PK_w_complaint] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_facilities]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_facilities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[STitle] [varchar](50) NOT NULL,
	[LxrName] [varchar](50) NOT NULL,
	[Phone] [varchar](11) NOT NULL,
	[ZYName] [varchar](50) NOT NULL,
	[Zbfilie] [varchar](50) NOT NULL,
	[Sexplain] [varchar](200) NULL,
 CONSTRAINT [PK_w_facilities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_floor]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_floor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FloorName] [varchar](20) NOT NULL,
	[FloorNumber] [int] NOT NULL,
	[FloorHeight] [decimal](18, 4) NOT NULL,
	[FloorMJ] [decimal](18, 4) NOT NULL,
	[CreationDate] [date] NOT NULL,
	[FloorType] [varchar](10) NOT NULL,
	[Remarks] [varchar](200) NULL,
 CONSTRAINT [PK_w_floor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_house_property]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_house_property](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomHao] [varchar](50) NULL,
	[NnitName] [varchar](50) NULL,
	[FloorNmae] [varchar](20) NULL,
	[Orientation] [varchar](10) NULL,
	[DecorationStandard] [varchar](50) NULL,
	[IsSell] [varchar](50) NULL,
	[HouseMJ] [decimal](18, 2) NULL,
 CONSTRAINT [PK_w_house_property] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_notice]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_notice](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NoticeCode] [varchar](50) NOT NULL,
	[NoticeTitle] [varchar](50) NOT NULL,
	[NoticeDate] [datetime] NOT NULL,
	[NoticeContent] [varchar](5000) NOT NULL,
	[NoticeName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_w_notice] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_Property]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_Property](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[WY_Name] [varchar](50) NULL,
	[Age] [int] NULL,
	[Sexs] [varchar](2) NULL,
	[Phone] [varchar](11) NULL,
	[WY_post] [varchar](50) NULL,
 CONSTRAINT [PK_w_Property] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_repair]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_repair](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BX_title] [varchar](50) NULL,
	[BX_Unit] [varchar](50) NULL,
	[BX_content] [varchar](50) NULL,
	[BX_date] [datetime] NULL,
	[BX_Handled_By] [varchar](50) NULL,
	[BX_Company] [varchar](50) NULL,
	[WX_State] [int] NULL,
	[WX_explain] [varchar](50) NULL,
 CONSTRAINT [PK_w_repair] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_room_info]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_room_info](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Phone] [varchar](11) NOT NULL,
	[Link_Name] [varchar](50) NOT NULL,
	[Stop_car_area] [int] NULL,
	[Construct_Data] [date] NULL,
	[Road_Area] [int] NULL,
	[Room_Ared] [int] NULL,
	[Lh_Area] [int] NULL,
	[Room_Number] [int] NULL,
	[Address] [varchar](50) NOT NULL,
	[Remark] [varchar](500) NULL,
 CONSTRAINT [PK_w_room_info] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[w_vehicle]    Script Date: 2023/1/28 23:34:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[w_vehicle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UsersId] [int] NOT NULL,
	[VCode] [varchar](50) NULL,
	[VName] [varchar](50) NULL,
	[VType] [varchar](50) NULL,
	[VStatas] [varchar](50) NULL,
 CONSTRAINT [PK_w_vehicle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[w_admin] ADD  CONSTRAINT [DF_w_Power]  DEFAULT ((0)) FOR [Power]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'UserName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房间号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'RoomHAO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'邮箱' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'Mian'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'所住楼宇' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'LY_info'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'账号余额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'AccountBalance'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'真姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_admin', @level2type=N'COLUMN',@level2name=N'NickName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'权限 0-业主  1-管理员' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_admin', @level2type=N'COLUMN',@level2name=N'Power'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_admin', @level2type=N'COLUMN',@level2name=N'CreaTetime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房间号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_charge', @level2type=N'COLUMN',@level2name=N'FJ_Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'应付金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_charge', @level2type=N'COLUMN',@level2name=N'YF_Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实付金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_charge', @level2type=N'COLUMN',@level2name=N'SF_Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收费类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_charge', @level2type=N'COLUMN',@level2name=N'SF_Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收费状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_charge', @level2type=N'COLUMN',@level2name=N'SF_State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'经手人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_charge', @level2type=N'COLUMN',@level2name=N'HandledBy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支出金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_chargedetailed', @level2type=N'COLUMN',@level2name=N'ZC_Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_chargedetailed', @level2type=N'COLUMN',@level2name=N'Remarks'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支出类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_chargedetailed', @level2type=N'COLUMN',@level2name=N'ZC_Type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'剩余金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_chargedetailed', @level2type=N'COLUMN',@level2name=N'Surplus
Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投诉标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_complaint', @level2type=N'COLUMN',@level2name=N'TS_title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投诉单元信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_complaint', @level2type=N'COLUMN',@level2name=N'TS_unit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投诉人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_complaint', @level2type=N'COLUMN',@level2name=N'TS_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_complaint', @level2type=N'COLUMN',@level2name=N'TS_date'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投诉内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_complaint', @level2type=N'COLUMN',@level2name=N'TS_content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'处理结果' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_complaint', @level2type=N'COLUMN',@level2name=N'Handle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'处理状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_complaint', @level2type=N'COLUMN',@level2name=N'CL_state'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设施标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_facilities', @level2type=N'COLUMN',@level2name=N'STitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_facilities', @level2type=N'COLUMN',@level2name=N'LxrName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_facilities', @level2type=N'COLUMN',@level2name=N'Phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'主要负责人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_facilities', @level2type=N'COLUMN',@level2name=N'ZYName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'周边设施类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_facilities', @level2type=N'COLUMN',@level2name=N'Zbfilie'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'设施说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_facilities', @level2type=N'COLUMN',@level2name=N'Sexplain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'楼盘名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_floor', @level2type=N'COLUMN',@level2name=N'FloorName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'层数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_floor', @level2type=N'COLUMN',@level2name=N'FloorNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'高度' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_floor', @level2type=N'COLUMN',@level2name=N'FloorHeight'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'面积' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_floor', @level2type=N'COLUMN',@level2name=N'FloorMJ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'创建时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_floor', @level2type=N'COLUMN',@level2name=N'CreationDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房间号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_house_property', @level2type=N'COLUMN',@level2name=N'RoomHao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单元名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_house_property', @level2type=N'COLUMN',@level2name=N'NnitName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'楼号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_house_property', @level2type=N'COLUMN',@level2name=N'FloorNmae'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'朝向' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_house_property', @level2type=N'COLUMN',@level2name=N'Orientation'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'装修标准' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_house_property', @level2type=N'COLUMN',@level2name=N'DecorationStandard'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否出售' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_house_property', @level2type=N'COLUMN',@level2name=N'IsSell'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'房产面积' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_house_property', @level2type=N'COLUMN',@level2name=N'HouseMJ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_notice', @level2type=N'COLUMN',@level2name=N'NoticeCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_notice', @level2type=N'COLUMN',@level2name=N'NoticeTitle'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_notice', @level2type=N'COLUMN',@level2name=N'NoticeDate'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公告发布人名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_notice', @level2type=N'COLUMN',@level2name=N'NoticeName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_Property', @level2type=N'COLUMN',@level2name=N'WY_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'年龄' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_Property', @level2type=N'COLUMN',@level2name=N'Age'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'性别' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_Property', @level2type=N'COLUMN',@level2name=N'Sexs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_Property', @level2type=N'COLUMN',@level2name=N'Phone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'职务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_Property', @level2type=N'COLUMN',@level2name=N'WY_post'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'标题' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_repair', @level2type=N'COLUMN',@level2name=N'BX_title'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'保修单元' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_repair', @level2type=N'COLUMN',@level2name=N'BX_Unit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'内容' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_repair', @level2type=N'COLUMN',@level2name=N'BX_content'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'编写处理人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_repair', @level2type=N'COLUMN',@level2name=N'BX_Handled_By'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'维修单位' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_repair', @level2type=N'COLUMN',@level2name=N'BX_Company'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_repair', @level2type=N'COLUMN',@level2name=N'WX_State'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'维修说明' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_repair', @level2type=N'COLUMN',@level2name=N'WX_explain'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'负责人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Link_Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'停车面积' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Stop_car_area'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建筑日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Construct_Data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'道路面积' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Road_Area'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'建筑面积' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Room_Ared'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'绿化面积' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Lh_Area'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'楼宇数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Room_Number'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_room_info', @level2type=N'COLUMN',@level2name=N'Address'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车位编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_vehicle', @level2type=N'COLUMN',@level2name=N'VCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车位名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_vehicle', @level2type=N'COLUMN',@level2name=N'VName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'车位类型' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_vehicle', @level2type=N'COLUMN',@level2name=N'VType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'w_vehicle', @level2type=N'COLUMN',@level2name=N'VStatas'
GO
USE [master]
GO
ALTER DATABASE [WuyeProject] SET  READ_WRITE 
GO

