﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.ReceptionControllers
{
	/// <summary>
	/// 前台首页信息
	/// </summary>
	public class HomePageController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();


		/// <summary>
		/// 首页信息
		/// </summary>
		/// <returns></returns>
		public ActionResult Home()
		{
			var data = db.w_room_info.FirstOrDefault();
			HomeModel model = new HomeModel();
			model.Name = data.Name;
			model.Remark = data.Remark;
			model.Road_Area = data.Road_Area;
			model.Room_Ared = data.Room_Ared;
			model.Room_Number = data.Room_Number;
			if (Session["UsersId"] != null)
			{
				model.Users = db.Users.Find(Convert.ToInt32(Session["UsersId"]));

			}
			return View(model);
		}

		[HttpGet]
		public ActionResult Login(string name, string phone)
		{
			var info = db.Users.Where(n => n.UserName == name && n.Phone == phone).FirstOrDefault();
			if (info != null)
			{
				Session["UsersId"] = info.Id;

			}
			return Redirect("Home");
		}

		/// <summary>
		/// 退出登录
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult TcLog()
		{
			Session["UsersId"] = null;
			return Content("<script>alert('退出成功');window.location.href='/HomePage/Home';</script>");
		}


		public class HomeModel : w_room_info
		{
			
			public Users Users { get; set; }
		}
	}
}