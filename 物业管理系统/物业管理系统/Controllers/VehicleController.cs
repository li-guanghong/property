﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	public class VehicleController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();

		// GET: Vehicle
		public ActionResult Index()
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			var info = db.w_vehicle.ToList();
			//获取所以业主信息；
			var data = new LoginController().GetUsersName();
			foreach (var item in info)
			{
				if (item.UsersId.HasValue == true)
				{
					if (data.TryGetValue(item.UsersId.Value, out string v))
					{
						item.UsersName = v;
					}
				}
				else
				{
					item.UsersName = "无";
				}
				if (item.UsersId < 0 || item.UsersId == null && item.VStatas != "空闲")
				{
					UpdateVstate(item.Id);
				}
			}
			info = db.w_vehicle.ToList();
			return View(info);
		}

		[HttpPost]
		public ActionResult GetCode(string title)
		{
			var info = db.w_vehicle.Where(N => N.VCode == title).ToList();
			var data = new LoginController().GetUsersName();
			foreach (var item in info)
			{
				if (item.UsersId.HasValue == true)
				{
					if (data.TryGetValue(item.UsersId.Value, out string v))
					{
						item.UsersName = v;
					}
				}
				else
				{
					item.UsersName = "无";
				}
				if (item.UsersId < 0 || item.UsersId == null && item.VStatas != "空闲")
				{
					UpdateVstate(item.Id);
				}
			}
			return View("Index", info);
		}
		public ActionResult UpdateVehicle(int Id)
		{
			w_vehicle info = db.w_vehicle.Find(Id);
			w_vehicles vehicles = new w_vehicles()
			{
				Id = info.Id,
				UsersName = info.UsersName,
				VCode = info.VCode,
				VName = info.VName,
				VStatas = info.VStatas,
				VType = info.VType,
				UsersNames = new LoginController().GetUsersName().Select(n => n.Value).ToList(),
			};
			return View(vehicles);
		}


		public ActionResult AddVehicle()
		{
			var data = new LoginController().GetUsersName();

			return View(data.Select(n => n.Value).ToList());
		}

		[HttpPost]
		public ActionResult AddVehicle(w_vehicle info)
		{
			var data = new LoginController().GetUsersName();
			w_vehicle vehicles = new w_vehicle()
			{
				UsersId = data.Where(N => N.Value == info.UsersName).Select(N => N.Key).FirstOrDefault(),
				VCode = info.VCode,
				VName = info.VName,
				VStatas = info.VStatas,
				VType = info.VType,
			};
			if (vehicles.UsersId.HasValue)
			{
				vehicles.VStatas = "已售卖";
			}
			else
			{
				vehicles.UsersId = null;
				vehicles.VStatas = "空闲";
			}
			db.w_vehicle.Add(vehicles);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/Vehicle/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新编辑！";
			}
			return Redirect("/Vehicle/Index");
		}


		[HttpPost]
		public ActionResult UpdateVehicle(w_vehicles info)
		{
			var data = new LoginController().GetUsersName();
			w_vehicle vehicles = new w_vehicle()
			{
				Id = info.Id,
				UsersId = data.Where(N => N.Value == info.UsersName).Select(N => N.Key).FirstOrDefault(),
				VCode = info.VCode,
				VName = info.VName,
				VStatas = info.VStatas,
				VType = info.VType,
			};
			if (vehicles.UsersId.HasValue)
			{
				vehicles.VStatas = "已售卖";
			}
			else
			{
				vehicles.UsersId = null;
				vehicles.VStatas = "空闲";
			}
			db.Entry(vehicles).State = System.Data.Entity.EntityState.Modified;
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('编辑成功');window.location.href='/Vehicle/Index';</script>");
			}
			else
			{
				ViewBag.notice = "编辑失败，请重新编辑！";
			}
			return Redirect("/Vehicle/Index");
		}


		/// <summary>
		/// 修改车位状态
		/// </summary>
		/// <param name="Id"></param>
		public void UpdateVstate(int Id)
		{
			var info = db.w_vehicle.Find(Id);
			info.VStatas = "空闲";
			db.SaveChanges();
		}

		// <summary>
		/// 删除楼产
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult DelVstate(int Id)
		{
			var result = db.w_vehicle.Find(Id);
			db.w_vehicle.Remove(result);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/Vstate/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
			}
			return Redirect("/Vstate/Index");


		}


		/// <summary>
		/// 业主车位信息
		/// </summary>
		/// <returns></returns>
		public ActionResult GetUserVehicle()
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			var info = db.w_vehicle.Where(n => n.UsersId == usersId).ToList();
			var usersNmae = db.Users.Find(usersId).UserName;
			foreach (var item in info)
			{
				item.UsersName = usersNmae;
			}

			return View(info);

		}

		public class w_vehicles
		{
			public int Id { get; set; }
			public string VCode { get; set; }
			public string VName { get; set; }
			public string VType { get; set; }
			public string VStatas { get; set; }
			public string UsersName { get; set; }

			public List<string> UsersNames { get; set; }
		}
	}
}