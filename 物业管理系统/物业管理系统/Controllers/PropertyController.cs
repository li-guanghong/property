﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 物业信息
	/// </summary>
	public class PropertyController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();

		public ActionResult Index()
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			return View(db.w_Property.ToList());
		}

		/// <summary>
		/// 修改物业信息
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ActionResult EditProperty(int id)
		{
			if (id == 0)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			w_Property info = db.w_Property.Find(id);
			if (info == null)
			{
				return HttpNotFound();
			}
			return View(info);
		}


		/// <summary>
		/// 修改物业信息
		/// </summary>
		/// <param name="users"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult EditProperty(w_Property r)
		{
			if (ModelState.IsValid)
			{
				db.Entry(r).State = EntityState.Modified;
				var resul = db.SaveChanges();
				if (resul > 0)
				{
					return Content("<script>alert('修改成功');window.location.href='/Property/Index';</script>");
				}
				else
				{
					ViewBag.notice = "修改失败，请重新编辑！";
				}
				return RedirectToAction("Index");
			}
			return View(r);
		}

		/// <summary>
		/// 添加物业信息
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		public ActionResult AddProperty(w_Property r)
		{
			db.w_Property.Add(r);
			var resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/Property/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新编辑！";
			}

			return View("Index");

		}

		/// <summary>
		/// 添加物业信息
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult AddProperty()
		{
			return View();
		}

		/// <summary>
		/// 获取物业信息列表
		/// </summary>
		/// <returns></returns>
		public ActionResult GetPropertyList()
		{
			return View(db.w_Property.ToList());
		}
	}
}