﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;
using 物业管理系统.Models;
using System.Text;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 登录信息控制器
	/// </summary>
	public class LoginController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();
		public ActionResult Index()
		{
			return View();
		}


		/// <summary>
		/// 登录
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult Login(string username, string password,string x)
		{
			
			var data = db.w_admin.FirstOrDefault(n => n.UserName == username && n.Pass == password && n.Power == 1);
			if (data == null)
			{
				ViewBag.notice = "账号或密码错误，请重新输入";
				return View("Index");
			}
			else
			{
				//记录登录用户信息
				Session["NickName"] = data.NickName;
				Session["UserName"] = data.UserName;
				IsRemembePaw(x, username, password);
				return Redirect("~/Admin/Index");//转跳
			}

		}

		/// <summary>
		/// 退出
		/// </summary>
		/// <returns></returns>
		public ActionResult Logout()
		{
			Session["NickName"] = null;
			Session["UserName"] = null;
			return Redirect("~/Login/Index");//转跳
		}

		/// <summary>
		/// 获取用户名称
		/// </summary>
		/// <returns></returns>
		public Dictionary<int, string> GetUsersName()
		{
			Dictionary<int, string> ks = new Dictionary<int, string>();
			var info = db.Users.ToList();
			foreach (var item in info)
			{
				ks.Add(item.Id, item.UserName);
			}
			return ks;
		}

		public  void IsRemembePaw(String x,string username, string password)
		{
			string fullPath = Server.MapPath("../")+""+ username + ".txt" ;
			StringBuilder sb = new StringBuilder();
			if (System.IO.File.Exists(fullPath))//存在
			{
				System.IO.File.Delete(fullPath);
				sb = sb. Append(username.Trim() + "+" + password.Trim());
				System.IO.File.WriteAllText(fullPath, sb.ToString());
			}
			else//不存在
			{
				
				sb = sb.Append(username.Trim() + "+" + password.Trim());
				System.IO.File.WriteAllText(fullPath, sb.ToString());
			}
		}
	}
}