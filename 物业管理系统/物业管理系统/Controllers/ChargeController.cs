﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 收费信息
	/// </summary>
	public class ChargeController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();

		public ActionResult Index()
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			var info = db.w_charge.ToList();
			foreach (var item in info)
			{
				if (item.YF_Amount - item.SF_Amount > 0)
				{
					item.SF_State = "未付清";
				}
				else
				{
					item.SF_State = "已付清";
				}
			}
			return View(info);
		}


		/// <summary>
		/// 查询
		/// </summary>
		/// <param name="charg"></param>
		/// <returns></returns>
		public ActionResult GetCharg(chargModel charg)
		{
			var info = db.w_charge.ToList();
			if (!string.IsNullOrEmpty(charg.P_Name))
			{
				info = info.Where(n => n.FJ_Number.StartsWith(charg.P_Name)).ToList();
			}
			if (!string.IsNullOrEmpty(charg.State) && charg.State != "请选择")
			{
				info = info.Where(n => n.SF_State == charg.State).ToList();
			}
			if (!string.IsNullOrEmpty(charg.Type) && charg.Type != "请选择")
			{
				info = info.Where(n => n.SF_Type == charg.Type).ToList();
			}
			if (charg.month > 0)
			{
				info = info.Where(n => n.SF_YF == charg.month).ToList();
			}
			return View("Index", info);
		}

		public class chargModel
		{
			/// <summary>
			/// 月份
			/// </summary>
			public int month { get; set; }
			/// <summary>
			/// 状态
			/// </summary>

			public string State { get; set; }
			/// <summary>
			/// 类型
			/// </summary>

			public string Type { get; set; }
			/// <summary>
			/// 楼盘名称
			/// </summary>
			public string P_Name { get; set; }



		}

		/// <summary>
		/// 添加费用
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult AddCharge()
		{
			List<string> info = db.Users.Select(n => n.LY_info + n.RoomHAO).ToList();
			return View(info);
		}

		[HttpPost]
		public ActionResult AddCharge(w_charge r)
		{
			r.HandledBy = Session["NickName"].ToString();
			r.SF_State = "未付清";
			r.SF_Amount = 0m;
			db.w_charge.Add(r);
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/Charge/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新输入";
			}
			return View("Index");
		}


		// <summary>
		/// 删除收费信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult DelCharge(int Id)
		{
			var result = db.w_charge.Find(Id);
			db.w_charge.Remove(result);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/Charge/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
			}
			return Redirect("/Charge/Index");
		}

		/// <summary>
		/// 获取业主收费信息
		/// </summary>
		/// <returns></returns>
		public ActionResult GetChargeByUsersIdList(int Id)
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			var unit = db.Users.Where(n => n.Id == usersId).Select(n => n.LY_info + n.RoomHAO).FirstOrDefault();//获取业主的小区地址信息
			var data = db.w_charge.Where(n => n.FJ_Number == unit).ToList();
			return View(data);

		}

		public ActionResult UpdateCharge(int Id)
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			var users = db.Users.Find(usersId);
			var charge = db.w_charge.Find(Id);
			var Wamount = charge.YF_Amount - charge.SF_Amount;
			if (InspectBalance(Wamount.Value) == false)
			{
				return Content("<script>alert('账号金额不足，请充值');window.location.href='/Charge/UsersRecharge';</script>");
			}
			else
			{
				//修改业主的账号金额和收费信息的金额
				users.AccountBalance = users.AccountBalance - Wamount;
				charge.SF_Amount = charge.YF_Amount;
				charge.SF_State = "已付清";
				db.Entry(users).State = System.Data.Entity.EntityState.Modified;
				db.Entry(charge).State = System.Data.Entity.EntityState.Modified;
				//添加一天收费详细信息
				db.w_chargedetailed.Add(new w_chargedetailed()
				{
					UsersId = usersId,
					Surplus_Amount = users.AccountBalance,
					ZC_Amount = charge.YF_Amount,
					ZC_Date = DateTime.Now,
					ZC_Type = "消费",
					Remarks = "无",
				});
				db.SaveChanges();

			}

			return Redirect("/HomePage/Home");
		}
		/// <summary>
		/// 检查余额是否
		/// </summary>
		/// <returns></returns>
		public bool InspectBalance(decimal  amount)
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			
			var users = db.Users.Find(usersId);
			var usersAmunt=users.AccountBalance;
			if (usersAmunt < amount)
			{
				//支付金额大于 及余额不足
				return false;

			}
			return true;



		}

		/// <summary>
		/// 用户充值
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult UsersRecharge()
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			var users = db.Users.Find(usersId);


			return View(users);

		}

		/// <summary>
		/// 用户充值
		/// </summary>
		/// <param name="amount"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult UsersRecharge(decimal amount)
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			var users = db.Users.Find(usersId);
			if (amount < 1)
			{
				return Content("<script>alert('充值金额不能为0，请充值');window.location.href='/Charge/UsersRecharge';</script>");
			}
			else
			{
				users.AccountBalance =+ amount;
				db.Entry(users).State = System.Data.Entity.EntityState.Modified;
				db.SaveChanges();
				return Content("<script>alert('充值成功');window.location.href='GetChargeByUsersIdList/"+ usersId + "';</script>");
			}

		}
	}
}