﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 金额详细
	/// </summary>
	public class ChargeDetailedController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();

		public ActionResult Index(int Id)
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			var info = db.w_chargedetailed.Where(n => n.UsersId == Id).ToList();
			List<ChargeDetailedModel> v = new List<ChargeDetailedModel>();
			foreach (var item in info)
			{
				v.Add(new ChargeDetailedModel()
				{
					Id = item.Id,
					Surplus_Amount = item.Surplus_Amount,
					ZC_Amount = item.ZC_Amount,
					Remarks = item.Remarks,
					ZC_Date = item.ZC_Date,
					ZC_Type = item.ZC_Type,
					UsersName = UsersName(Id)
				});
			}
			return View(v);
		}

		/// <summary>
		/// 获取用户名称
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public string UsersName(int Id)
		{
			return db.Users.Select(N => N.UserName).FirstOrDefault();
		}

		public class ChargeDetailedModel : w_chargedetailed
		{
			public string UsersName { get; set; }
		}

	}
}