﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{

	/// <summary>
	/// 楼产信息
	/// </summary>
	public class HouseController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();
	
		public ActionResult Index()
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			var info = db.w_house_property.ToList();
			return View(info);
		}

		/// <summary>
		/// 添加楼产
		/// </summary>
		/// <returns></returns>
		public ActionResult AddHouse()
		{
			var info = db.w_floor.Select(n => n.FloorName).ToList();
			return View(info);
		}

		/// <summary>
		/// 添加楼产
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddHouse(w_house_property r)
		{
			db.w_house_property.Add(r);
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/House/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新输入";
			}
			return Redirect("/House/Index");
		}


		/// <summary>
		/// 修改楼产
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult UpdateHouse(int Id)
		{
			var resulu = db.w_house_property.Find(Id);
			resulu.FjName = db.w_floor.Select(n => n.FloorName).ToList();
			return View(resulu);
		}

		/// <summary>
		/// 修改楼产
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult UpdateHouse(w_house_property r)
		{
			db.Entry(r).State = System.Data.Entity.EntityState.Modified;
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('修改成功');window.location.href='/House/Index';</script>");
			}
			else
			{
				ViewBag.notice = "修改失败，请重新修改";
			}
			return View("Index");
		}


		// <summary>
		/// 删除楼产
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult DelHouse(int Id)
		{
			var result = db.w_house_property.Find(Id);
			db.w_house_property.Remove(result);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/House/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
			}
			return Redirect("/House/Index");


		}

		/// <summary>
		/// 获取楼
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>

		[HttpPost]
		public ActionResult GetHouse(string title)
		{
			var info = db.w_house_property.ToList();
			if (!string.IsNullOrEmpty(title))
			{
				info = db.w_house_property.Where(n => n.RoomHao.StartsWith(title)).ToList();
			}
			return View("Index", info);
		}

		
	}
}