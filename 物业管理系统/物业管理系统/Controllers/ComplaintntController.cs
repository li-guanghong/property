﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 投诉信息控制器
	/// </summary>
	public class ComplaintntController : Controller
    {
		/// <summary>
		/// 投诉信息
		/// </summary>
        private WuyeProjectEntities db = new WuyeProjectEntities();

        public ActionResult Index()
        {
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			List< w_complaint> info = db.w_complaint.ToList();
            return View(info);
        }

		/// <summary>
		/// 修改投诉信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult UpdateComplaintnt(int Id)
		{
            var info = db.w_complaint.Find(Id);
			return View(info);
		}

		/// <summary>
		/// 修改投诉信息
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
        public ActionResult UpdateComplaintnt(w_complaint r)
		{
			var info = db.w_complaint.Find(r.Id);
			info.CL_state = r.CL_state;
			info.Handle = r.Handle;
			db.Entry(info).State = System.Data.Entity.EntityState.Modified;
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('处理成功');window.location.href='/Complaintnt/Index';</script>");
			}
			else
			{
				ViewBag.notice = "处理失败，请重新编辑！";
			}
			return Redirect("/Complaintnt/Index");
		}


		/// <summary>
		/// 删除投诉信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult DelComplaintnt(int Id)
		{
			var result = db.w_complaint.Find(Id);
			db.w_complaint.Remove(result);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/Complaintnt/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
			}
			return Redirect("/Complaintnt/Index");
		}


		/// <summary>
		/// 添加
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult AddComplaintnt()
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			return View();
		}

		/// <summary>
		/// 添加投诉
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddComplaintnt(w_complaint w)
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return View("/HomePage/Home");
			}
			var unit = db.Users.Where(n => n.Id == usersId).Select(n => n.LY_info + n.RoomHAO).FirstOrDefault();//获取业主的小区地址信息
			var name = db.Users.Where(n => n.Id == usersId).Select(n => n.UserName).FirstOrDefault();
			w.TS_unit = unit;
			w.TS_date = DateTime.Now;
			w.CL_state = 1;
			w.TS_Name = name;
			db.w_complaint.Add(w);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('你的投诉信息已提交');window.location.href='/Complaintnt/GetComplaintntList/ " + Convert.ToInt32(Session["UsersId"]) + "';</script>");
			}
			else
			{
				ViewBag.notice = "你的投诉信息失败，请重新编辑！";
			}
			return Redirect("/Complaintnt/GetComplaintntList");

		}

		/// <summary>
		/// 获取投诉信息列表
		/// </summary>
		/// <returns></returns>
		public ActionResult GetComplaintntList()
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			var info = db.Users.Find(Convert.ToInt32(Session["UsersId"]));
			var unit = info.LY_info + info.RoomHAO;
			var data = db.w_complaint.Where(n => n.TS_unit == unit).ToList();
			

			return View(data);
		}

	}
}