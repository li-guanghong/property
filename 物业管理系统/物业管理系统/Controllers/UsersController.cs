﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	public class UsersController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();

		// GET: Users
		public ActionResult Index()
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			return View(db.Users.ToList());
		}

		// GET: Users/Details/5
		public ActionResult Details(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Users users = db.Users.Find(id);
			if (users == null)
			{
				return HttpNotFound();
			}
			return View(users);
		}

		// GET: Users/Create
		public ActionResult Create()
		{
			return View();
		}

		// POST: Users/Create
		// 为了防止“过多发布”攻击，请启用要绑定到的特定属性。有关
		// 详细信息，请参阅 https://go.microsoft.com/fwlink/?LinkId=317598。
		[HttpPost]
		public ActionResult Create([Bind(Include = "Id,UserName,RoomHAO,Phone,Mian,LY_info,AccountBalance")] Users users)
		{
			if (ModelState.IsValid)
			{
				db.Users.Add(users);
				var resul = db.SaveChanges();
				if (resul > 0)
				{
					return Content("<script>alert('修改成功');window.location.href='/Users/Index';</script>");
				}
				else
				{
					ViewBag.notice = "修改失败，请重新编辑！";
				}
				return RedirectToAction("Index");
			}

			return View(users);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			Users users = db.Users.Find(id);
			if (users == null)
			{
				return HttpNotFound();
			}
			return View(users);
		}

		/// <summary>
		/// 修改业主信息
		/// </summary>
		/// <param name="users"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult Edit(Users users)
		{
			if (ModelState.IsValid)
			{
				db.Entry(users).State = EntityState.Modified;
				var resul = db.SaveChanges();
				if (resul > 0)
				{
					return Content("<script>alert('修改成功');window.location.href='/Users/Index';</script>");
				}
				else
				{
					ViewBag.notice = "修改失败，请重新编辑！";
				}
				return RedirectToAction("Index");
			}
			return View(users);
		}

		/// <summary>
		/// 删除信息
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			return new UsersController().DeleteConfirmed(id.Value);

		}

		public ActionResult DeleteConfirmed(int id)
		{
			Users users = db.Users.Find(id);
			db.Users.Remove(users);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}

		[HttpPost]
		public ActionResult AddUsers(Users r)
		{
			db.Users.Add(r);
			var resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('修改成功');window.location.href='/Users/Index';</script>");
			}
			else
			{
				ViewBag.notice = "修改失败，请重新编辑！";
			}

			return View("Index");

		}

		[HttpGet]
		public ActionResult AddUsers()
		{
			return View();
		}

		/// <summary>
		/// 查询某个业主
		/// </summary>
		/// <returns></returns>
		public ActionResult GetUsers(int Id)
		{
			if (Id < 1)
			{
				return View("/HomePage/Home");
			}
			var info = db.Users.Find(Id);

			UsersModel users = new UsersModel();
			users.Id = info.Id;
			users.UsersName = info.UserName;
			users.QB_Amount = info.AccountBalance.Value;

			users.Address = info.LY_info+ info.RoomHAO;
			var C = db.w_charge.Where(n => n.FJ_Number == users.Address);
			users.WZF_Amount =C .Sum(n => n.YF_Amount.Value - n.SF_Amount.Value);
			users.TS_Info= db.w_repair.Where(n => n.BX_Unit == users.Address).ToDictionary(n=>n.Id,n=>n.BX_title);
			return View(users);
		}




		public class UsersModel
		{
			public int Id { get; set; }
			/// <summary>
			/// 业主姓名
			/// </summary>
			public string UsersName { get; set; }
			/// <summary>
			/// 地址
			/// </summary>

			public string Address { get; set; }
			/// <summary>
			/// 全部金额
			/// </summary>

			public Decimal QB_Amount { get; set; }
			/// <summary>
			/// 未支付金额
			/// </summary>
			public Decimal WZF_Amount { get; set; }
			/// <summary>
			/// 投诉信息
			/// </summary>
			public Dictionary<int, string> TS_Info { get; set; }


		}
	}
}
