﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
    /// <summary>
    /// 周边设施
    /// </summary>
    public class FacilitiesController : Controller
    {
        private WuyeProjectEntities db = new WuyeProjectEntities();

        // GET: Facilities
        public ActionResult Index()
        {
            //检查是否登录
            if (Session["UserName"] == null)
            {
                return Redirect("~/Login/Index");//转跳
            }
            return View(db.w_facilities.ToList());
        }

       /// <summary>
       /// 删除设施信息
       /// </summary>
       /// <param name="Id"></param>
       /// <returns></returns>
        public ActionResult DelFacilities(int Id)
        {
            db.w_facilities.Remove(db.w_facilities.Find(Id));
            var resul = db.SaveChanges();
            if (resul > 0)
            {
                return Content("<script>alert('删除成功');window.location.href='/Facilities/Index';</script>");
            }
            else
            {
                ViewBag.notice = "删除失败，请重新删除！";
                return View("Index");
            }
        }


		/// <summary>
		/// 标题查询
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult GetTitle(string title)
		{
			var info = db.w_facilities.ToList();
			if (!string.IsNullOrEmpty(title))
			{
				info = db.w_facilities.Where(n => n.STitle.StartsWith(title)).ToList();
			}
			return View("Index", info);
		}
		
		/// <summary>
		/// 添加设施信息
		/// </summary>
		/// <returns></returns>
		public ActionResult AddFacilities()
		{
			return View();
		}

		/// <summary>
		/// 添加设施信息
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddFacilities(w_facilities r)
		{
			db.w_facilities.Add(r);
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/Facilities/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新输入";
			}
			return Redirect("/Facilities/Index");
		}

		/// <summary>
		/// 修改设施信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult UpdateFacilities(int Id)
		{
			var data = db.w_facilities.Find(Id);
			if (data == null)
			{
				return Content("<script>alert('没有找到设施信息，请先添加');window.location.href='/Facilities/AddFacilities';</script>");
			}
			return View(data);
		}

		/// <summary>
		/// 修改设施信息
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult UpdateFacilities(w_facilities r)
		{
			db.Entry(r).State = System.Data.Entity.EntityState.Modified;
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('编辑成功');window.location.href='/Facilities/Index';</script>");
			}
			else
			{
				ViewBag.notice = "编辑失败，请重新编辑！";
			}
			return Redirect("/Facilities/Index");
		}

		/// <summary>
		/// 获取设施信息列表
		/// </summary>
		/// <returns></returns>
		public ActionResult GetFacilitiesList()
		{
			return View(db.w_facilities.ToList());
		}

		/// <summary>
		/// 获取设施根据标题
		/// </summary>
		/// <param name="keyword"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult GetFacilitiesByTltie(string keyword)
		{
			var info = db.w_facilities.ToList();
			if (!string.IsNullOrEmpty(keyword))
			{
				info = info.Where(n => n.STitle == keyword).ToList();
			}
			return View("GetFacilitiesList", info);
		}

		/// <summary>
		/// 获取某个设施信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult GetFacilitiesById(int Id)
		{
			var data = db.w_facilities.Find(Id);
			return View(data);
		}
	}
}