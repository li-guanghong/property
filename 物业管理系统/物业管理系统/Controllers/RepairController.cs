﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 报修
	/// </summary>
	public class RepairController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();

		// GET: Repair
		public ActionResult Index()
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			List<w_repair> info = db.w_repair.ToList();
			return View(info);
		}


		public ActionResult UpdateRepair(int Id)
		{
			var info = db.w_repair.Find(Id);
			return View(info);
		}


		[HttpPost]
		public ActionResult UpdateRepair(w_repair r)
		{
			var info = db.w_repair.Find(r.Id);
			info.WX_State = r.WX_State;
			info.WX_explain = r.WX_explain;
			info.BX_Company = r.BX_Company;
			info.BX_Handled_By = r.BX_Handled_By;

			db.Entry(info).State = System.Data.Entity.EntityState.Modified;
			int resul = db.SaveChanges();

			if (resul > 0)
			{
				return Content("<script>alert('处理成功');window.location.href='/Repair/Index';</script>");
			}
			else
			{
				ViewBag.notice = "处理失败，请重新编辑！";
			}
			return Redirect("/Repair/Index");
		}


		[HttpGet]
		public ActionResult DelRepair(int Id)
		{
			var result = db.w_repair.Find(Id);
			db.w_repair.Remove(result);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/Repair/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
			}
			return Redirect("/Repair/Index");
		}


		/// <summary>
		/// 添加报修
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public ActionResult AddRepair()
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			return View();
		}


		/// <summary>
		/// 添加报修
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddRepair(w_repair w)
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return Redirect("/HomePage/Home");
			}
			var unit = db.Users.Where(n => n.Id == usersId).Select(n => n.LY_info + n.RoomHAO).FirstOrDefault();//获取业主的小区地址信息
			w.BX_Unit = unit;
			w.BX_date = DateTime.Now;
			w.WX_State = 1;
			db.w_repair.Add(w);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('你的报修信息已提交');window.location.href='/Users/GetUsers/ " + Convert.ToInt32(Session["UsersId"]) + "';</script>");
			}
			else
			{
				ViewBag.notice = "报修信息失败，请重新编辑！";
			}
			return Redirect("/Users/GetUsers");

		}

		


		public ActionResult GetRepairList(int? a)
		{
			var usersId = Convert.ToInt32(Session["UsersId"]);
			if (usersId < 1)
			{
				return View("/HomePage/Home");
			}
			var info = db.Users.Find(Convert.ToInt32(Session["UsersId"]));
			var unit = info.LY_info + info.RoomHAO;
			var data = db.w_repair.Where(n => n.BX_Unit == unit).ToList();
			if (a.HasValue)
			{
				data = data.Where(n => n.Id == a.Value).ToList();
			}
			
			return View(data);
		}

	}
}