﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 小区信息
	/// </summary>
	public class AdminController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();
		// GET: Admin
		public ActionResult Index()
		{

			//检查是否登录
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			return View();
		}


		/// <summary>
		/// 小区基本信息
		/// </summary>
		/// <returns></returns>
		public ActionResult RoomIndex()
		{
			var data = db.w_room_info.FirstOrDefault();
			return View(data);
		}


		/// <summary>
		/// 添加小区
		/// </summary>
		/// <returns></returns>
		public ActionResult AddRoom()
		{
			return View();
		}

		/// <summary>
		/// 添加小区
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddRoom(w_room_info r)
		{
			db.w_room_info.Add(r);
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('保存成功');window.location.href='/Admin/RoomIndex';</script>");
			}
			else
			{
				ViewBag.notice = "保存失败，请重新输入";
			}
			return View();
		}


		/// <summary>
		/// 编辑小区信息
		/// </summary>
		/// <returns></returns>
		public ActionResult UpdateRoom()
		{
			var data = db.w_room_info.FirstOrDefault();
			if (data == null)
			{
				return Content("<script>alert('没有找到小区信息，请先添加');window.location.href='/Admin/AddRoom';</script>");

			}
			return View(data);
		}


		/// <summary>
		/// 编辑小区信息
		/// </summary>
		/// <returns></returns>
		[HttpPost]
		public ActionResult UpdateRoom(w_room_info r)
		{
			db.Entry(r).State = System.Data.Entity.EntityState.Modified;
			var result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('编辑成功');window.location.href='/Admin/RoomIndex';</script>");
			}
			else
			{
				ViewBag.notice = "编辑失败，请重新输入";
			}
			return View();
		}
	}
}