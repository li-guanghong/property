﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
	/// <summary>
	/// 公告
	/// </summary>
	public class NoticeController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();

		
		public ActionResult Index()
		{
			//检查是否登录
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			return View(db.w_notice.ToList());
		}

		/// <summary>
		/// 获取公告列表
		/// </summary>
		/// <returns></returns>
		public ActionResult GetNoticeList()
		{
			return View(db.w_notice.ToList());
		}

		/// <summary>
		/// 获取某个公告信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult GetNoticeById(int Id)
		{
			var data = db.w_notice.Find(Id);
			
			return View(data);
		}

		/// <summary>
		/// 获取公告根据标题
		/// </summary>
		/// <param name="keyword"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult GetNoticeByTltie(string keyword)
		{
			var info = db.w_notice.ToList();
			if (!string.IsNullOrEmpty(keyword))
			{
				info = info.Where(n => n.NoticeTitle == keyword).ToList();
			}
			return View("GetNoticeList", info);
		}

		/// <summary>
		/// 标题查询
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult GetTitle(string title)
		{
			var info = db.w_notice.ToList();
			if (!string.IsNullOrEmpty(title))
			{
				info = db.w_notice.Where(n => n.NoticeTitle.StartsWith(title)).ToList();
			}
			return View("Index", info);
		}

		/// <summary>
		/// 添加公告
		/// </summary>
		/// <returns></returns>
		public ActionResult AddNotice()
		{
			return View();
		}

		/// <summary>
		/// 添加公告信息
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddNotice(w_notice r)
		{
			//检查是否登录
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			r.NoticeCode = "G" + DateTime.Now.ToString("yyMMddHHmmss");
			r.NoticeDate = DateTime.Now;
			r.NoticeName = Session["NickName"].ToString();
			db.w_notice.Add(r);
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/Notice/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新输入";
			}
			return Redirect("/Notice/Index");
		}


		/// <summary>
		/// 修改公告信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult UpdateNotice(int Id)
		{
			var data = db.w_notice.Find(Id);
			if (data == null)
			{
				return Content("<script>alert('没有找到公告信息，请先添加');window.location.href='/Notice/AddNotice';</script>");
			}
			return View(data);
		}

		/// <summary>
		/// 修改公告
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult UpdateNotice(w_notice r)
		{
			db.Entry(r).State = System.Data.Entity.EntityState.Modified;
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('编辑成功');window.location.href='/Notice/Index';</script>");
			}
			else
			{
				ViewBag.notice = "编辑失败，请重新编辑！";
			}
			return Redirect("/Notice/Index");
		}

		/// <summary>
		/// 删除
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult DelNotice(int Id)
		{


			db.w_notice.Remove(db.w_notice.Find(Id));
			var resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/Notice/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
				return View("Index");
			}
		}

	}
}