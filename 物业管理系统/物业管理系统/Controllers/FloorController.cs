﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{

	/// <summary>
	/// 楼宇信息
	/// </summary>
	public class FloorController : Controller
	{
		private WuyeProjectEntities db = new WuyeProjectEntities();
		public ActionResult Index()
		{
			if (Session["UserName"] == null)
			{
				return Redirect("~/Login/Index");//转跳
			}
			return View(db.w_floor.ToList());

		}

		/// <summary>
		/// 删除楼层
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult DelFloor(int Id)
		{
			var result = db.w_floor.Find(Id);
			db.w_floor.Remove(result);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/Floor/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
			}
			return Redirect("/Floor/Index");


		}

		/// <summary>
		/// 添加公告
		/// </summary>
		/// <returns></returns>
		public ActionResult AddFloor()
		{
			return View();
		}

		/// <summary>
		/// 添加楼层
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddFloor(w_floor r)
		{
			db.w_floor.Add(r);
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/Floor/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新输入";
			}
			return Redirect("/Floor/Index");
		}

		/// <summary>
		/// 批量添加楼层
		/// </summary>
		/// <returns></returns>
		public ActionResult BatchAddFloor()
		{
			return View();
		}

		/// <summary>
		/// 批量添加楼层
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult BatchAddFloor(w_floors r)
		{
			var number = (r.jsnumber+1) - r.ksnumber;
			List<w_floor> ws = new List<w_floor>();
			for (int i = 0; i <number; i++)
			{
				ws.Add(new w_floor()
				{
					CreationDate = r.CreationDate,
					FloorHeight = r.FloorHeight,
					FloorMJ = r.FloorMJ,
					FloorName = (r.ksnumber+i)+r.FloorName,
					FloorNumber = r.FloorNumber,
					FloorType = r.FloorType,
					Remarks = r.Remarks
				});
			}
			foreach (var item in ws)
			{
				var h = db.w_floor.Where(n => n.FloorName == item.FloorName).FirstOrDefault();
				if (h != null)
				{
					return Content("<script>alert('添加失败，楼层名称存在');window.location.href='/Floor/Index';</script>");
				}
				db.w_floor.Add(item);
			}

			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('添加成功');window.location.href='/Floor/Index';</script>");
			}
			else
			{
				ViewBag.notice = "添加失败，请重新输入";
			}
			return Redirect("/Floor/Index");
		}

		/// <summary>
		/// 修改楼层
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult UpdateFloor(int Id)
		{
			return View(db.w_floor.Find(Id));

		}

		/// <summary>
		/// 修改楼层
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult UpdateFloor(w_floor r)
		{
			db.Entry(r).State = System.Data.Entity.EntityState.Modified;
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('编辑成功');window.location.href='/Floor/Index';</script>");
			}
			else
			{
				ViewBag.notice = "编辑失败，请重新编辑！";
			}
			return Redirect("/Floor/Index");
		}

		/// <summary>
		/// 获取楼层信息
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>
		public ActionResult GetFloor(string title)
		{
			var info = db.w_floor.ToList();
			if (!string.IsNullOrEmpty(title))
			{
				info = db.w_floor.Where(n => n.FloorName.StartsWith(title)).ToList();
			}
			return View("Index", info);
		}


		public partial class w_floors
		{
			public int Id { get; set; }
			public string FloorName { get; set; }
			public int FloorNumber { get; set; }
			public decimal FloorHeight { get; set; }
			public decimal FloorMJ { get; set; }
			public System.DateTime CreationDate { get; set; }
			public string FloorType { get; set; }
			public string Remarks { get; set; }

			public int ksnumber { get; set; }
			public int jsnumber { get; set; }
		}
	}
}