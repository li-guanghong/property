﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using 物业管理系统.Models;

namespace 物业管理系统.Controllers
{
    public class WadminController : Controller
    {
        private WuyeProjectEntities db = new WuyeProjectEntities();

        // GET: Wadmin
        public ActionResult Index()
        {
            var info = db.w_admin.Where(n => n.Power == 1).ToList();
            return View(info);
        }



		/// <summary>
		/// 添加管理员信息
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
        public ActionResult AddWadmin(w_admin r)
		{
			//检查账号是否存在
			var a = db.w_admin.Where(n => n.UserName == r.UserName).FirstOrDefault().Id;
			if (a > 0)
			{
				ViewBag.notice = "保存失败，账号存在，请重新输入";
				return View("Index");
			}

			r.CreaTetime = DateTime.Now;
			r.Power = 1;
			db.w_admin.Add(r);
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('保存成功');window.location.href='/Wadmin/Index';</script>");
			}
			else
			{
				ViewBag.notice = "保存失败，请重新输入";
			}
			return View("Index");


		}


		/// <summary>
		/// 修改管理员信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult UpdateWadmin(int Id)
		{
			var resulu = db.w_admin.Find(Id);
			return View(resulu);
		}
		

		/// <summary>
		/// 修改管理员密码
		/// </summary>
		/// <param name="r"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult UpdateWadmin(w_admin r)
		{
			r.Power = 1;
			db.Entry(r).State = System.Data.Entity.EntityState.Modified;
			int result = db.SaveChanges();
			if (result > 0)
			{
				return Content("<script>alert('修改成功');window.location.href='/Wadmin/Index';</script>");
			}
			else
			{
				ViewBag.notice = "修改失败，请重新修改";
			}
			return View("Index");
		}


		/// <summary>
		/// 删除管理员信息
		/// </summary>
		/// <param name="Id"></param>
		/// <returns></returns>
		public ActionResult DelWadmin(int Id)
		{
			w_admin info = db.w_admin.Find(Id);
			db.w_admin.Remove(info);
			int resul = db.SaveChanges();
			if (resul > 0)
			{
				return Content("<script>alert('删除成功');window.location.href='/Wadmin/Index';</script>");
			}
			else
			{
				ViewBag.notice = "删除失败，请重新删除！";
			}
			return Redirect("Index");
		}
	}
}